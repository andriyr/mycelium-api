syntax = "proto3";

package api;

service Node {
    rpc Node (NodeRequest) returns (NodeResponse) {};
    rpc CreateNeuronCluster (CreateNeuronClusterRequest) returns (NeuronClusterResponse) {};
    rpc DeleteNeuronCluster (DeleteNeuronClusterRequest) returns (DeleteNeuronClusterResponse) {};
    rpc SetNeuronClusterPosition (SetNeuronClusterPositionRequest) returns (SetNeuronClusterPositionResponse) {};
    rpc CreateSynapseCluster (CreateSynapseClusterRequest) returns (SynapseClusterResponse) {};
    rpc DeleteSynapseCluster (DeleteSynapseClusterRequest) returns (DeleteSynapseClusterResponse) {};
    rpc AttachSynapseCluster (AttachSynapseClusterRequest) returns (AttachSynapseClusterResponse) {};
    rpc DetachSynapseCluster (DetachSynapseClusterRequest) returns (DetachSynapseClusterResponse) {};
    rpc SetSynapseClusterPosition (SetSynapseClusterPositionRequest) returns (SetSynapseClusterPositionResponse) {};
    rpc CreateSTDPPreceptor (CreateSTDPPreceptorRequest) returns (STDPPreceptorResponse) {};
    rpc DeleteSTDPPreceptor (DeleteSTDPPreceptorRequest) returns (DeleteSTDPPreceptorResponse) {};
    rpc AttachSTDPPreceptor (AttachSTDPPreceptorRequest) returns (AttachSTDPPreceptorResponse) {};
    rpc DetachSTDPPreceptor (DetachSTDPPreceptorRequest) returns (DetachSTDPPreceptorResponse) {};
    rpc SetSTDPPreceptorPosition (SetSTDPPreceptorPositionRequest) returns (SetSTDPPreceptorPositionResponse) {};
    rpc CreateModulatedSTDPPreceptor (CreateModulatedSTDPPreceptorRequest) returns (ModulatedSTDPPreceptorResponse) {};
    rpc DeleteModulatedSTDPPreceptor (DeleteModulatedSTDPPreceptorRequest) returns (DeleteModulatedSTDPPreceptorResponse) {};
    rpc AttachModulatedSTDPPreceptor (AttachModulatedSTDPPreceptorRequest) returns (AttachModulatedSTDPPreceptorResponse) {};
    rpc DetachModulatedSTDPPreceptor (DetachModulatedSTDPPreceptorRequest) returns (DetachModulatedSTDPPreceptorResponse) {};
    rpc SetModulatedSTDPPreceptorPosition (SetModulatedSTDPPreceptorPositionRequest) returns (SetModulatedSTDPPreceptorPositionResponse) {};
    rpc CreateInputAdapter (CreateInputAdapterRequest) returns (InputAdapterResponse) {};
    rpc DeleteInputAdapter (DeleteInputAdapterRequest) returns (DeleteInputAdapterResponse) {};
    rpc AttachInputAdapter (AttachInputAdapterRequest) returns (AttachInputAdapterResponse) {};
    rpc DetachInputAdapter (DetachInputAdapterRequest) returns (DetachInputAdapterResponse) {};
    rpc SetInputAdapterPosition (SetInputAdapterPositionRequest) returns (SetInputAdapterPositionResponse) {};
    rpc CreateOutputAdapter (CreateOutputAdapterRequest) returns (OutputAdapterResponse) {};
    rpc DeleteOutputAdapter (DeleteOutputAdapterRequest) returns (DeleteOutputAdapterResponse) {};
    rpc AttachOutputAdapter (AttachOutputAdapterRequest) returns (AttachOutputAdapterResponse) {};
    rpc DetachOutputAdapter (DetachOutputAdapterRequest) returns (DetachOutputAdapterResponse) {};
    rpc SetOutputAdapterPosition (SetOutputAdapterPositionRequest) returns (SetOutputAdapterPositionResponse) {};
    rpc CreateDopamineAdapter (CreateDopamineAdapterRequest) returns (DopamineAdapterResponse) {};
    rpc DeleteDopamineAdapter (DeleteDopamineAdapterRequest) returns (DeleteDopamineAdapterResponse) {};
    rpc AttachDopamineAdapter (AttachDopamineAdapterRequest) returns (AttachDopamineAdapterResponse) {};
    rpc DetachDopamineAdapter (DetachDopamineAdapterRequest) returns (DetachDopamineAdapterResponse) {};
    rpc SetDopamineAdapterPosition (SetDopamineAdapterPositionRequest) returns (SetDopamineAdapterPositionResponse) {};
    rpc StreamInput (stream StreamInputRequest) returns (StreamInputResponse) {};
    rpc StreamOutput (StreamOutputRequest) returns (stream StreamOutputResponse) {};
    rpc StreamDopamine (stream StreamDopamineRequest) returns (StreamDopamineResponse) {};
    rpc StreamNeuronSpikeEvent (StreamNeuronSpikeEventRequest) returns (stream NeuronSpikeEvent) {};
    rpc StreamNeuronSpikeRelayEvent (StreamNeuronSpikeRelayEventRequest) returns (stream NeuronSpikeRelayEvent) {};
    rpc StreamSynapseWeightUpdateEvent (StreamSynapseWeightUpdateEventRequest) returns (stream SynapseWeightUpdateEvent) {};
}

message NodeRequest {

}

message NodeResponse {
    repeated NeuronClusterResponse neuronClusters = 1;
    repeated SynapseClusterResponse synapseClusters = 2;
    repeated STDPPreceptorResponse STDPPreceptors = 3;
    repeated ModulatedSTDPPreceptorResponse modulatedSTDPPreceptors = 4;
    repeated InputAdapterResponse inputAdapters = 5;
    repeated OutputAdapterResponse outputAdapters = 6;
    repeated DopamineAdapterResponse dopamineAdapters = 7;
}

message NeuronClusterRequest {
    string id = 1;
}

message NeuronClusterResponse {
    string id = 1;
    string name = 2;
    int32 size = 3;
    double ratio = 4;
    repeated string neuronIds = 5;
    repeated string inputAdapterIds = 6;
    Graph graph = 7;
}

message CreateNeuronClusterRequest {
    string name = 1;
    int32 size = 2;
    double ratio = 3;
    Graph graph = 4;
}

message DeleteNeuronClusterRequest {
    string id = 1;
}

message DeleteNeuronClusterResponse {
    string status = 1;
}

message SetNeuronClusterPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetNeuronClusterPositionResponse {
    string status = 1;
}

message SynapseClusterRequest {
    string id = 1;
}

message SynapseClusterResponse {
    string id = 1;
    string name = 2;
    int32 neuronsCovered = 3;
    double initialSynapseWeight = 4;
    repeated string neuronClusterIds = 5;
    Graph graph = 6;
}

message CreateSynapseClusterRequest {
    string name = 1;
    int32 neuronsCovered = 2;
    double initialSynapseWeight = 3;
    Graph graph = 4;
}

message DeleteSynapseClusterRequest {
    string id = 1;
}

message DeleteSynapseClusterResponse {
    string status = 1;
}

message AttachSynapseClusterRequest {
    string synapseClusterId = 1;
    string neuronClusterId = 2;
}

message AttachSynapseClusterResponse {
    string status = 1;
}

message DetachSynapseClusterRequest {
    string synapseClusterId = 1;
    string neuronClusterId = 2;
}

message DetachSynapseClusterResponse {
    string status = 1;
}

message SetSynapseClusterPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetSynapseClusterPositionResponse {
    string status = 1;
}

message STDPPreceptorRequest {
    string id = 1;
}

message STDPPreceptorResponse {
    string id = 1;
    string name = 2;
    int32 maxQueueSize = 3;
    string synapseClusterId = 4;
    Graph graph = 5;
}

message CreateSTDPPreceptorRequest {
    string name = 1;
    int32 maxQueueSize = 2;
    Graph graph = 3;
}

message DeleteSTDPPreceptorRequest {
    string id = 1;
}

message DeleteSTDPPreceptorResponse {
    string status = 1;
}

message AttachSTDPPreceptorRequest {
    string STDPPreceptorId = 1;
    string synapseClusterId = 2;
}

message AttachSTDPPreceptorResponse {
    string status = 1;
}

message DetachSTDPPreceptorRequest {
    string STDPPreceptorId = 1;
}

message DetachSTDPPreceptorResponse {
    string status = 1;
}

message SetSTDPPreceptorPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetSTDPPreceptorPositionResponse {
    string status = 1;
}

message ModulatedSTDPPreceptorRequest {
    string id = 1;
}

message ModulatedSTDPPreceptorResponse {
    string id = 1;
    string name = 2;
    int32 maxQueueSize = 3;
    double sensitivity = 4;
    string synapseClusterId = 5;
    string dopamineAdapterId = 6;
    Graph graph = 7;
}

message CreateModulatedSTDPPreceptorRequest {
    string name = 1;
    int32 maxQueueSize = 2;
    double sensitivity = 3;
    Graph graph = 4;
}

message DeleteModulatedSTDPPreceptorRequest {
    string id = 1;
}

message DeleteModulatedSTDPPreceptorResponse {
    string status = 1;
}

message AttachModulatedSTDPPreceptorRequest {
    string modulatedSTDPPreceptorId = 1;
    string synapseClusterId = 2;
}

message AttachModulatedSTDPPreceptorResponse {
    string status = 1;
}

message DetachModulatedSTDPPreceptorRequest {
    string modulatedSTDPPreceptorId = 1;
}

message DetachModulatedSTDPPreceptorResponse {
    string status = 1;
}

message SetModulatedSTDPPreceptorPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetModulatedSTDPPreceptorPositionResponse {
    string status = 1;
}

message InputAdapterRequest {
    string id = 1;
}

message InputAdapterResponse {
    string id = 1;
    string name = 2;
    int32 size = 3;
    int32 encodingWindow = 4;
    Graph graph = 5;
}

message CreateInputAdapterRequest {
    string name = 1;
    int32 size = 2;
    int32 encodingWindow = 3;
    Graph graph = 4;
}

message DeleteInputAdapterRequest {
    string id = 1;
}

message DeleteInputAdapterResponse {
    string status = 1;
}

message AttachInputAdapterRequest {
    string inputAdapterId = 1;
    string neuronClusterId = 2;
}

message AttachInputAdapterResponse {
    string status = 1;
}

message DetachInputAdapterRequest {
    string inputAdapterId = 1;
    string neuronClusterId = 2;
}

message DetachInputAdapterResponse {
    string status = 1;
}

message SetInputAdapterPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetInputAdapterPositionResponse {
    string status = 1;
}

message OutputAdapterRequest {
    string id = 1;
}

message OutputAdapterResponse {
    string id = 1;
    string name = 2;
    int32 size = 3;
    int32 decodingWindow = 4;
    string neuronClusterId = 5;
    Graph graph = 6;
}

message CreateOutputAdapterRequest {
    string name = 1;
    int32 size = 2;
    int32 decodingWindow = 3;
    Graph graph = 4;
}

message DeleteOutputAdapterRequest {
    string id = 1;
}

message DeleteOutputAdapterResponse {
    string status = 1;
}

message AttachOutputAdapterRequest {
    string outputAdapterId = 1;
    string neuronClusterId = 2;
}

message AttachOutputAdapterResponse {
    string status = 1;
}

message DetachOutputAdapterRequest {
    string outputAdapterId = 1;
}

message DetachOutputAdapterResponse {
    string status = 1;
}

message SetOutputAdapterPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetOutputAdapterPositionResponse {
    string status = 1;
}

message DopamineAdapterRequest {
    string id = 1;
}

message DopamineAdapterResponse {
    string id = 1;
    string name = 2;
    Graph graph = 3;
}

message CreateDopamineAdapterRequest {
    string name = 1;
    Graph graph = 2;
}

message DeleteDopamineAdapterRequest {
    string id = 1;
}

message DeleteDopamineAdapterResponse {
    string status = 1;
}

message AttachDopamineAdapterRequest {
    string dopamineAdapterId = 1;
    string modulatedSTDPPreceptorId = 2;
}

message AttachDopamineAdapterResponse {
    string status = 1;
}

message DetachDopamineAdapterRequest {
    string dopamineAdapterId = 1;
    string modulatedSTDPPreceptorId = 2;
}

message DetachDopamineAdapterResponse {
    string status = 1;
}

message SetDopamineAdapterPositionRequest {
    string id = 1;
    Graph graph = 2;
}

message SetDopamineAdapterPositionResponse {
    string status = 1;
}

message StreamInputRequest {
    string inputAdapterId = 1;
    repeated double values = 2;
}

message StreamInputResponse {
    string status = 1;
}

message StreamOutputRequest {
    string outputAdapterId = 1;
}

message StreamOutputResponse {
    string outputAdapterId = 1;
    repeated double values = 2;
}

message StreamDopamineRequest {
    string dopamineAdapterId = 1;
    double dopamine = 2;
}

message StreamDopamineResponse {
    string status = 1;
}

enum Component {
    UNKNOWN = 0;
    NEURON_CLUSTER = 1;
    SYNAPSE_CLUSTER = 2;
    STDP_PRECEPTOR = 3;
    MODULATED_STDP_PRECEPTOR = 4;
    INPUT_ADAPTER = 5;
    OUTPUT_ADAPTER = 6;
    DOPAMINE_ADAPTER = 7;
}

message StreamNeuronSpikeEventRequest {
    string id = 1;
    Component component = 2;
    int32 samplingInterval = 3;
}

message NeuronSpikeEvent {
    string neuronClusterId = 1;
    string neuronId = 2;
}

message StreamNeuronSpikeRelayEventRequest {
    string id = 1;
    Component component = 2;
    int32 samplingInterval = 3;
}

message NeuronSpikeRelayEvent {
    string neuronClusterId = 1;
    string neuronId = 2;
    double potential = 3;
}

message StreamSynapseWeightUpdateEventRequest {
    string id = 1;
    Component component = 2;
    int32 samplingInterval = 3;
}

message SynapseWeightUpdateEvent {
    string PreSynapticNeuronId = 1;
    string PostSynapticNeuronId = 2;
    double WeightDelta = 3;
}

message Graph {
    double x = 1;
    double y = 2;
}
